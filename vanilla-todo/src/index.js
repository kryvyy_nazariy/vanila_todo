import './index.css';
import createMarkUp from './App';
import  './components/gettingData.js';
import  './components/enterNewTask.js';
import  editTask from './components/editTask.js';
import './components/closeTask.js';
import routing from './components/routing.js';



document.addEventListener("DOMContentLoaded", function(event) {
   
    const heading = createMarkUp();
    const indexBlock = document.getElementById('root');
    console.log(indexBlock);
    indexBlock.innerHTML = heading;
    
}); 

window.onload = function () { 
    routing();    
    editTask();
};

