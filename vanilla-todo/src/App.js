// import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css'; 

function createMarkUp(){
  let finalHtml='';
  
  //create nav
  const header = document.querySelector('.header');  
  let mainMenuArray = [{name:'Greetigs page', link:'/greetigs_page'},
                       {name:'To do list', link:'/to_do_list'}
                      ];  
  header.querySelector('ul').innerHTML = mainMenuArray
  .map(item =>(`<li><a href=${item.link}>${item.name}</a></li>`))
  .join('');
  //create nav                      
  var heading = `<h1>ToDo List</h1>`;
  finalHtml+=heading;
  return finalHtml;
}

export default createMarkUp;
