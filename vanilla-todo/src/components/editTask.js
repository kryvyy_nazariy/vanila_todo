import saveData from "./saveChanges.js";
import getAllTasks from "./getAllTasks.js";

function editTask() {
    let currentTasksList ;
   
    document.addEventListener('click', function(e){
        if(e.target.matches('.edit')){
            e.preventDefault();
            let button = e.target;
            let closestRow = button.closest("tr");
            let siblingInput = closestRow.querySelector("input");
            var currentCondition = button.dataset.condition;

            if (currentCondition === "edit") {
                button.dataset.condition = "save";
                button.innerText = "Save";
                siblingInput.disabled = false;
                siblingInput.classList.add("editable");
            } else if (currentCondition === "save") {
                button.dataset.condition = "edit";
                button.innerText = "Edit";
                siblingInput.disabled = true;
                siblingInput.classList.remove("editable");
                
                currentTasksList = getAllTasks();
             
                console.log(currentTasksList);
                // saveData(tasksList);
            }
        }
    }); 
}

export default editTask;





