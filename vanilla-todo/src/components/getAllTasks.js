
function getAllTasks(newItem){
     let editInputs = Array.from(document.querySelectorAll("table input"));

     function task(text, number) {
       this.number = number;
       this.text = text;
     }
     let tasksList = [];
     editInputs.forEach((input, index) => {
       tasksList.push(new task(input.value, index + 1));
     });
     if (newItem) {
        tasksList.push(newItem);
     }
     return tasksList;
}

export default getAllTasks;