//create table
function updateTable(marckup){
    let table_row_html = document.querySelector('table').innerHTML;
    let table_row_array = marckup;
    //array example [{number: 1, text: 'create smthg'}, {number:2, text: 'make home work'}];
    let table_row = document.getElementById('table-row').innerHTML;
    let table = document.querySelector("table");
    let tbody = Array.from(table.querySelectorAll("tbody"));

    if(table.querySelector('tbody')){
        tbody.forEach(currentItem => {
            table.removeChild(currentItem);
        });
        table_row_html = document.querySelector("table").innerHTML;        
    }   
    table_row_html +=  table_row_array.map(row => table_row
        .replace(/{{number}}/g, row.number)
        .replace(/{{text-value}}/g, row.text)
    ).join('');
    document.querySelector('table').innerHTML = table_row_html;
}
//create table 

export default updateTable;