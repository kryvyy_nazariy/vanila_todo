import updateTable from './updateTable.js';

let tasksList = null;

fetch('./tasks.json')  
.then(function(response) {  
  return response.text();  
})  
.then(function(data) {   
  JSON.parse(data).map((item) => tasksList = item.tasksArray);
  console.log(tasksList); 
  updateTable(tasksList);  
})  
.catch(function(error) {  
  console.log('Request failed', error) ;
});

export default tasksList;