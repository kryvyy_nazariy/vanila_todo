
function routing(){
    const navlinks = Array.from(document.querySelectorAll('header a'));
    const greetings = document.querySelector('.greating');
    const todoList = document.querySelector('.todo_list');

    if (window.location.pathname === '/greetigs_page') {
            todoList.style.display = 'none';
            greetings.animate([{opacity: 0},{opacity: 1}], {duration: 600, fill: 'forwards' });
            todoList.animate([{ opacity: 1 }, { opacity: 0 }], { duration: 600, fill: 'forwards' });
    } else if (window.location.pathname === '/to_do_list') {
            greetings.animate([{opacity: 1},{opacity: 0}], { duration: 600,fill: 'forwards' });
            todoList.animate([{opacity: 0},{opacity: 1}], { duration: 600,fill: 'forwards' });
    }
    setTimeout(function() {
        todoList.style.display = 'block';
    }, 600);

    navlinks.forEach(link => {
        link.addEventListener('click', function(e){
            e.preventDefault();
            let currentLink = link.getAttribute('href');
            window.history.pushState(null, null ,currentLink);
            window.dispatchEvent(new Event("popstate"));
           
        });    
    });

    window.addEventListener("popstate", function(e){
        console.log(e.target.location.pathname);
        if (e.target.location.pathname === '/greetigs_page') { 
            greetings.animate([{opacity: 0},{opacity: 1}], {duration: 600, fill: 'forwards' }).onFinish = todoList.animate([{ opacity: 1 }, { opacity: 0 }], { duration: 600, fill: 'forwards' });;            
            
        } else if (e.target.location.pathname === '/to_do_list') {             
            todoList.animate([{opacity: 0},{opacity: 1}], { duration: 600,fill: 'forwards' }).onFinish = greetings.animate([{opacity: 1},{opacity: 0}], { duration: 600,fill: 'forwards' });  
            
        }
    }, false);
}

export default routing;
