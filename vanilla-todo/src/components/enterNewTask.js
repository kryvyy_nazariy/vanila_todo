import updateTable from './updateTable.js';
import getAllTasks from "./getAllTasks.js";

const form = document.querySelector('.new-task');
const formButton = form.querySelector('button');
const inputField = form.querySelector('input');
const table = document.querySelector('table');


let newTask = [{
    number: 0,
    text: ''
}];

formButton.addEventListener('click', (e)=>{
    e.preventDefault();
    let countTasks = (table.querySelectorAll('input')).length;    
    let inputValue = inputField.value;
    newTask[0].number = countTasks + 1;
    newTask[0].text = inputValue;
    newTask = getAllTasks(newTask[0]);
    updateTable(newTask);
    inputField.value ='';
    // tasksList.push(newTask);
});