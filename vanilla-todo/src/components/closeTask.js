document.addEventListener('click', function(e){
    e.preventDefault();
    if(e.target.matches('.close')){
        let closeButton = e.target;
        let currentRow = closeButton.closest('tr');
        let tBody = currentRow.closest('tbody');
        let table = currentRow.closest('table');
        
        tBody.removeChild(currentRow);
        let rows = Array.from(table.querySelectorAll('tr'));
        rows.forEach((row, index) => {
            if(index > 0){
                let currentCell = row.querySelector('td');
                currentCell.innerText = index;
            }
            
        });


    }
})